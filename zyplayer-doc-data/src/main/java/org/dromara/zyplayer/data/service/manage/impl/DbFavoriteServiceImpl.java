package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.DbFavorite;
import org.dromara.zyplayer.data.repository.manage.mapper.DbFavoriteMapper;
import org.dromara.zyplayer.data.service.manage.DbFavoriteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-08-21
 */
@Service
public class DbFavoriteServiceImpl extends ServiceImpl<DbFavoriteMapper, DbFavorite> implements DbFavoriteService {

}
