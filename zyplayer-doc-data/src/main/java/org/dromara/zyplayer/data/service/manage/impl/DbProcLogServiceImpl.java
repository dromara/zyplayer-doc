package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.DbProcLog;
import org.dromara.zyplayer.data.repository.manage.mapper.DbProcLogMapper;
import org.dromara.zyplayer.data.service.manage.DbProcLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 数据库函数修改日志 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-04-26
 */
@Service
public class DbProcLogServiceImpl extends ServiceImpl<DbProcLogMapper, DbProcLog> implements DbProcLogService {

}
