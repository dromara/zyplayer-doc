package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.UserGroupRelation;
import org.dromara.zyplayer.data.repository.manage.mapper.UserGroupRelationMapper;
import org.dromara.zyplayer.data.service.manage.UserGroupRelationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户和用户组关系表 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-02-08
 */
@Service
public class UserGroupRelationServiceImpl extends ServiceImpl<UserGroupRelationMapper, UserGroupRelation> implements UserGroupRelationService {

}
