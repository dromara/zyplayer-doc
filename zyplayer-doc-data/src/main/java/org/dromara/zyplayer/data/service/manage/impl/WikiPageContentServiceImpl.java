package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.WikiPageContent;
import org.dromara.zyplayer.data.repository.manage.mapper.WikiPageContentMapper;
import org.dromara.zyplayer.data.service.manage.WikiPageContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-02-24
 */
@Service
public class WikiPageContentServiceImpl extends ServiceImpl<WikiPageContentMapper, WikiPageContent> implements WikiPageContentService {

}
