package org.dromara.zyplayer.data.service.manage;

import org.dromara.zyplayer.data.repository.manage.entity.DbProcLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 数据库函数修改日志 服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-04-26
 */
public interface DbProcLogService extends IService<DbProcLog> {

}
