package org.dromara.zyplayer.data.repository.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dromara.zyplayer.data.repository.manage.entity.BackupTask;

/**
 *  备份任务Mapper 接口
 *
 * @author diantu
 * @since 2023-03-03
 */
public interface BackupTaskMapper extends BaseMapper<BackupTask> {

}
