package org.dromara.zyplayer.data.service.manage.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.dromara.zyplayer.data.repository.manage.entity.UserInfo;
import org.dromara.zyplayer.data.repository.manage.mapper.UserInfoMapper;
import org.dromara.zyplayer.data.service.manage.UserInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-03
 */
@Service
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements UserInfoService {

}
