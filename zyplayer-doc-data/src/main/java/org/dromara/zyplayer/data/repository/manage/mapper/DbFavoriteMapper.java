package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.DbFavorite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-08-21
 */
public interface DbFavoriteMapper extends BaseMapper<DbFavorite> {

}
