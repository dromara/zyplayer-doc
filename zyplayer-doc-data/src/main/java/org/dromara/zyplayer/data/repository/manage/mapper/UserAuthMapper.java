package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.UserAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户权限表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-05-31
 */
public interface UserAuthMapper extends BaseMapper<UserAuth> {

}
