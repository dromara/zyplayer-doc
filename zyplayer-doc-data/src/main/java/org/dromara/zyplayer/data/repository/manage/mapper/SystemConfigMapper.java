package org.dromara.zyplayer.data.repository.manage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.dromara.zyplayer.data.repository.manage.entity.SystemConfig;

/**
 * <p>
 * 系统配置表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2022-12-01
 */
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {

}
