package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.ApiCustomParams;
import org.dromara.zyplayer.data.repository.manage.mapper.ApiCustomParamsMapper;
import org.dromara.zyplayer.data.service.manage.ApiCustomParamsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 自建接口文档 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2022-01-29
 */
@Service
public class ApiCustomParamsServiceImpl extends ServiceImpl<ApiCustomParamsMapper, ApiCustomParams> implements ApiCustomParamsService {

}
