package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.ApiDoc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * api文档地址 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-11-25
 */
public interface ApiDocMapper extends BaseMapper<ApiDoc> {

}
