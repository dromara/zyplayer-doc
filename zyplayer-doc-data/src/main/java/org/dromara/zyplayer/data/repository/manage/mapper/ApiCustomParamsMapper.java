package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.ApiCustomParams;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 自建接口文档 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2022-01-29
 */
public interface ApiCustomParamsMapper extends BaseMapper<ApiCustomParams> {

}
