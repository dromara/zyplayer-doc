package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.WikiPageZan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2019-03-05
 */
public interface WikiPageZanMapper extends BaseMapper<WikiPageZan> {

}
