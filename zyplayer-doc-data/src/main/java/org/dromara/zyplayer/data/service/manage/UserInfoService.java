package org.dromara.zyplayer.data.service.manage;

import com.baomidou.mybatisplus.extension.service.IService;
import org.dromara.zyplayer.data.repository.manage.entity.UserInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2018-12-03
 */
public interface UserInfoService extends IService<UserInfo> {

}
