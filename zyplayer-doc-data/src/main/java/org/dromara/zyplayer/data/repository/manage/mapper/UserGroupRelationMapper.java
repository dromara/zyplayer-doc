package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.UserGroupRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和用户组关系表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-02-08
 */
public interface UserGroupRelationMapper extends BaseMapper<UserGroupRelation> {

}
