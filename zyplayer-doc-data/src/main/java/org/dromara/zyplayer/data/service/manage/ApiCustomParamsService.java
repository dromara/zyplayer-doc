package org.dromara.zyplayer.data.service.manage;

import org.dromara.zyplayer.data.repository.manage.entity.ApiCustomParams;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 自建接口文档 服务类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2022-01-29
 */
public interface ApiCustomParamsService extends IService<ApiCustomParams> {

}
