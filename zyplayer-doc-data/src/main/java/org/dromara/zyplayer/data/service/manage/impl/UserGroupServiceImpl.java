package org.dromara.zyplayer.data.service.manage.impl;

import org.dromara.zyplayer.data.repository.manage.entity.UserGroup;
import org.dromara.zyplayer.data.repository.manage.mapper.UserGroupMapper;
import org.dromara.zyplayer.data.service.manage.UserGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户组 服务实现类
 * </p>
 *
 * @author 暮光：城中城
 * @since 2021-02-08
 */
@Service
public class UserGroupServiceImpl extends ServiceImpl<UserGroupMapper, UserGroup> implements UserGroupService {

}
