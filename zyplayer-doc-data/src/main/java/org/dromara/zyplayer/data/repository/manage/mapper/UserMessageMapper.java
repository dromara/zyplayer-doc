package org.dromara.zyplayer.data.repository.manage.mapper;

import org.dromara.zyplayer.data.repository.manage.entity.UserMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户消息表 Mapper 接口
 * </p>
 *
 * @author 暮光：城中城
 * @since 2020-06-23
 */
public interface UserMessageMapper extends BaseMapper<UserMessage> {

}
