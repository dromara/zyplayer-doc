package org.dromara.zyplayer.manage.web.vo;

import lombok.*;

/**
 * 用户权限信息
 *
 * @author 暮光：城中城
 * @since 2018-12-15
 */
@Data
public class UserAuthVo {
	private boolean userManage;
}
